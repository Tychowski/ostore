import React, { Component } from 'react'
import { Container, Row, Col } from 'reactstrap';
import Select from 'react-select'




const options = [
    {   
        value: '1', 
        label: 'Czy wszystkie towary z oferty na stronie internetowej są dostępne ?', 
        text: 'Staramy się, aby zawsze tak właśnie było. Może się jednak zdarzyć, że zamówiony towar jest chwilowo niedostępny. W takich przypadkach, możliwie najszybciej informujemy Klienta o zaistniałej sytuacji i przedstawiamy ofertę, która sprosta jego oczekiwaniom i pozwoli na realizację zamówienia.' 
    },
    { 
        value: '2', 
        label: 'Kto pokrywa koszty dostawy ?', 
        text: 'Koszty dostawy pokrywa Sprzedający. Każde zamówienie jest wysyłane przez InPost Paczkomaty lub List Polecony' 
    },
    { 
        value: '3', 
        label: 'Kto pokrywa koszt zwrotnej przesyłki ?', 
        text: 'Jeżeli klient życzy wymienić produkt na inny kolor/rozmiar/wzór bo stwierdzi, że zmienił zdanie, przesyłke opłaca Kupujący' 
    },
    { 
        value: '4', 
        label: 'Co mogę zrobić jeśli otrzymam towar niezgodny z zamówieniem (model, rozmiar, ilość) ?', 
        text: 'Jeśli po otrzymaniu przesyłki stwierdzono jakiekolwiek rozbieżności z zamówieniem prosimy o jak najszybszy kontakt. W uzasadnionych sytuacjach koszt zwrotu towaru niezgodnego z zamówieniem pokrywa sklep.' 
    },
    { 
        value: '5', 
        label: 'Czy możliwy jest osobisty odbiór towaru ?', 
        text: 'Niestety nie ma takiej możliwości. Prowadzimy wyłącznie sprzedaż wysyłkową.' 
    },
    { 
        value: '6', 
        label: 'Czy wystawiacie fakturę VAT za zakupiony towar ?', 
        text: 'Tak, wystawiamy faktury VAT. Ceny w sklepie są cenami brutto.' 
    },
    { 
        value: '7', 
        label: 'Czy mogę dostać dodatkowy rabat ?', 
        text: 'Niestety nie możemy zaproponować niższych cen  niż te ceny , które  widać na stronie sklepu. Ceny w sklepie na bieżąco aktualizujemy i staramy się aby były jak najniższe.' 
    },
    { 
        value: '8', 
        label: 'Z jakich form płatności można skorzystać zamawiając towar w sklepie internetowym ?', 
        text: 'Płatności można dokonać tylko za pomocą karty płatniczej nap. visa / mastercard. W przyszłości dodamy inne sposoby opłaty' 
    }
];



  
class Question extends Component {
    state = {
      selectedOption: [],
    };

    handleChange = selectedOption => {
      this.setState({ selectedOption });
      console.log(`Option selected:`, selectedOption);
    };

    render() {

      const { selectedOption } = this.state;
    
      return (
        <>
            <Container>
                <Row>
                    <Col>
                        <h1 className="title">Najczęściej zadawane pytania</h1>
                        <Select
                            id="postTypeiuhg"
                            instanceId="hsdjsnchcsnich"
                            value={selectedOption}
                            onChange={this.handleChange}
                            options={options}
                            placeholder="Tu znajdziesz liste pytań"
                        />
                        <div className="content" onClick={this.handleChange}>
                            <p className="contentItem">
                                {selectedOption.text}
                            </p>
                        </div>
                    </Col>
                </Row>
            </Container>
            <style jsx>{`

                #postTypeiuhg {
                    border: 1px solid red !important;
                }

                .title {
                    display: flex; 
                    justify-content: center;
                    margin-bottom: 80px;
                    color: rgba(255, 0, 76, 0.822);
                }

                .content {
                    margin-top: 20px;
                    padding: 10px;
                }

                .contentItem {
                    font-weight: 400;
                    font-style: italic;
                }

            `}</style>
        </>
      );
    }
}

export default Question;