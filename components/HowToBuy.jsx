import React from 'react';
import { Container, Row, Col } from 'reactstrap';


const HowToBuy = () => {

  return (
    <>
        <Container>
            <Row>
                <Col>
                    <div className="whatSize">
                        <div className="contactTitle">
                            <h1>Dlaczego my?</h1>
                        </div>
                        <div className="orderInfo">
                            <h5>Zamówienia są wysyłane wciągu 24h</h5>
                        </div>
                        <div className="blockWhatSize">
                            <div className="imageKubik">
                                <img className="kubik" src="./whyWe.png" />
                            </div>
                            <div className="items">
                                <h6>1. Niska cena</h6><br />
                                <h6>2. Dobra jakość</h6><br />
                                <h6>3. Idealne na prezent</h6><br />
                                <h6>4. Nie płacisz za dostawe</h6><br />
                                <h6>5. Szybka wysyłka</h6><br />
                                <h6>6. Możliwość zwrotu</h6><br />
                                <h6>7. Wyśmienity wygląd</h6><br />
                            </div>
                        </div>
                        {/* <div className="contentContact">
                            <img className="imgTellSize" src="./tell-size.jpg" />
                        </div>
                        <div className="contentContactSmall">
                            <img className="imgTellSizeSmall" src="./small-tell-size.png" />
                        </div> */}
                    </div>
                </Col>
            </Row>
        </Container>
                                            
            
        <style jsx>{`

            .whatSize {
                margin: 150px 0px 100px;
            }

            .blockWhatSize {
                display: block;
                
            }

            .imageKubik {
                display: none;
            }

            .contactTitle {
                display: flex;
                justify-content: center;
                text-align: center;
                margin-top: 50px;
                margin-bottom: 30px;
                color: rgba(255, 0, 76, 0.822);
            }

            .imgTellSize {
                background-size: cover;
                width: 100%;
                border-radius: 10px;
            }

            .imgTellSizeSmall {
                background-size: cover;
                width: 100%;
                border-radius: 10px;
            }

            .contentContact {
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                justify-content: center;
                align-items: center;
                align-content: center;

                margin-top: 50px;
                text-align: center;
            }

            .contentContactSmall {
                display: none;
            }

            .orderInfo {
                display: flex;
                justify-content: center;

                text-align: center; 
                padding: 20px 5px 15px;
                margin-bottom: 20px;
                color: rgb(45, 138, 76);
                background: rgba(33, 204, 90, 0.178);
            }

            @media only screen and (min-width: 992px) {
                .blockWhatSize {
                    display: flex;
                    flex-direction: row;
                    flex-wrap: nowrap;
                    justify-content: space-around;
                    align-items: center;
                    align-content: center;
                }
    
                .imageKubik {
                    display: block;
                    width: 150px;
                }
    
                .kubik {
                    width: 350px;
                    border-radius: 10px;
                }
            }

            @media only screen and (min-width: 700px) {
                .items {
                    margin-left: 5%;
                }
            }

            @media only screen and (max-width: 550px) {
                .contentContact {
                    display: none;
                }
    
                .contentContactSmall {
                    display: flex;
                    flex-direction: row;
                    flex-wrap: nowrap;
                    justify-content: center;
                    align-items: center;
                    align-content: center;
    
                    margin-top: 50px;
                    text-align: center;
                } 
            }

        `}</style>
    </>
  );
}

export default HowToBuy;