import React from "react";
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import Router from "next/router";


const Footer = () => {
  return (
    <>
        <MDBFooter color="stylish-color-dark" className="font-small pt-4 mt-4">
            <MDBContainer fluid className="text-center text-md-left">
                <MDBRow>
                    <MDBCol md="6">
                        <h5 className="title">Ostore</h5>
                        <p className="footerContent">
                            Kładziemy nacisk na rozwój naszej produkcji
                        </p>
                        <p className="footerContent">
                            Dzięki któremu możliwe jest tworzenie wysokiej jakości bielizny.
                        </p>
                    </MDBCol>
                    <MDBCol md="6">
                        <h5 className="title">Social media</h5>
                        <div className="iconItems">
                            <p className="list-unstyled">
                                <a href="https://www.instagram.com/ostore.polska/">
                                    <img className="socialIcon" src="./icon.ico/instagram.png" width="35" />
                                </a>
                            </p>
                            <p className="list-unstyled">
                                <a href="https://www.facebook.com/ostore.polska">
                                    <img className="socialIcon" src="./icon.ico/facebook.png" width="35" />
                                </a>
                            </p>
                            <p className="list-unstyled">
                                <a href="mailto:ostore.polska@gmail.com">
                                    <img className="socialIcon" src="./gmail.png" width="35" />
                                </a>
                            </p>
                        </div>
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
            <div className="footer-copyright text-center py-3">
                <MDBContainer fluid>
                &copy; {new Date().getFullYear()} Copyright: <a href="/"> o-store.pl </a>
                </MDBContainer>
            </div>
            <div className="footerBottom">
                <div className="btnPolicy" onClick={() => Router.push("/policy")}>
                    Regulamin
                </div>
                <div className="btnPolicy" onClick={() => Router.push("/shiping")}>
                    Dostawa i płatność
                </div>
            </div>
        </MDBFooter>
        <style jsx>{`

            .font-small, h5 {
                font-weight: 100;
            }
          
            .title {
                color: rgba(255, 0, 76);
                text-align: center;
            }

            .footerContent {
                text-align: center;
                font-weight: 100;
            }

            .list-unstyled {
                text-align: center;
                margin: 0px 20px;
            }

            .iconItems {
                display: flex; 
                justify-content: center;
                margin: 20px;
            }

            .socialIcon {
                // margin: 5px;
            }

            .footerBottom {
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                justify-content: center;
                align-items: center;

                width: 100%;
                background: #22282e;
                height: 30px;
                font-size: 14px;
            }

            .btnPolicy {
                cursor: pointer;
                font-weight: 100;
                margin: 0px 50px;
            }

            .footer-copyright {
                font-weight: 100;
            }

            @media only screen and (max-width: 992px) {
                .changeSize {
                font-size: 16px;
                }  
            }

            @media only screen and (max-width: 600px) {
                .btnPolicy {
                    margin: 0px 20px;
                }
            }

        `}</style>
    </>
  );
};

export default Footer;


