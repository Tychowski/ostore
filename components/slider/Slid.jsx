import React from 'react';
import { UncontrolledCarousel } from 'reactstrap';

const items = [
  {
    src: './slider/1.jpg',
    altText: 'Slide 1',
    caption: 'O - store',
    header: 'Zadbaj o swój wygląd',
    key: '1'
  },
  {
    src: './slider/2.jpg',
    altText: 'Slide 2',
    caption: 'O - store',
    header: 'Powiększ swój "arsenal" ',
    key: '2'
  },
  {
    src: './slider/3.jpg',
    altText: 'Slide 3',
    caption: 'O - store',
    header: 'Podreśl piękno swego ciała',
    key: '3'
  },
  {
    src: './slider/4.jpg',
    altText: 'Slide 2',
    caption: 'O - store',
    header: 'Jego uwaga w twojej potędze',
    key: '4'
  },
  {
    src: './slider/5.jpg',
    altText: 'Slide 2',
    caption: 'O - store',
    header: 'Wyśmienity wygląd',
    key: '5'
  },
  {
    src: './slider/6.jpg',
    altText: 'Slide 2',
    caption: 'O - store',
    header: 'Pewność siebie',
    key: '6'
  },
  {
    src: './slider/7.jpg',
    altText: 'Slide 2',
    caption: 'O - store',
    header: 'Wygoda i komfort',
    key: '7'
  }
];

const Example = () => <UncontrolledCarousel items={items} />;

export default Example;