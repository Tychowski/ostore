import Head from "next/head";
import styled from "@emotion/styled";
import GlobalStyles from "./prebuilt/GlobalStyles";

import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";


const stripePromise = loadStripe("pk_live_51I9sePLYJnsmTdPN8Y6uvmgpIKH67njQ6ggvchMOvuW6thAB0gbjAIcXkarpZmjqEZ3ubFSj0B3fhtxQG180MKAr00tal8OHeg");

const Layout = ({ children, title }) => {


  return (
    <>
      <GlobalStyles />
      <Head>
        <title>{title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link key="iuj56" rel="icon" type="image/png" sizes="32x32" href="./icon.ico/favicon.ico" />
        <link rel="manifest" href="/icon.ico/manifest.json"></link>
        <link key="iuj568564" href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet" />
      </Head>
      <Elements stripe={stripePromise}>{children}</Elements>
    </>
  );
};

export default Layout;
