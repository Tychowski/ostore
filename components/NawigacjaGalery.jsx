import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';

import 'bootstrap/dist/css/bootstrap.css';
import { Container } from 'reactstrap';
import Router from "next/router";


const Nawigacja = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <>
      {/* <Navbar color="dark" dark expand="md"> */}
      <Navbar style={{backgroundColor: '#ffb2c5cc', color: '#333'}} dark expand="md">
        <Container>
        <NavbarBrand href="/" className="logoTitle">
          <img src="./ostoreHead.png" className="logoImg" />
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem className="navItem">
              <NavLink href="#"></NavLink>
            </NavItem>
            <NavItem className="navItem">
              <NavLink href="#"></NavLink>
            </NavItem>
            <NavItem className="navItem">
              <NavLink onClick={() => Router.push("/")}>
                <span className="navItem">Główna</span>
              </NavLink>
            </NavItem>
          </Nav>
          <NavbarText>
            <span className="navItemRight">
              Komfort i Wygoda 
              <img src="./icon.ico/feathers2.png" width='50' />
            </span>
          </NavbarText>
        </Collapse>
        </Container>
      </Navbar>

      <style jsx>{`

        .navItem {
          color: #fff;
          padding: 5px 10px;
        }

        .navItem:hover {
          color: rgba(255, 255, 255, 0.774);
          background:  rgba(255, 0, 85, 0.082);
        }

        .navItemRight {
          color: #d1607c;
          font-size: 14px;
          background: rgba(255, 255, 255, 0.774);
          padding: 8px 5px;
          font-style: italic;
          border-radius: 2px;
        }

        .navbar-custom {
          background-color: green;
        }

        .logoImg {
          width: 150px;
        }

        .logoTitle {
          font-size: 50px !important;
          color: red;
        }
          
        .navItems {
          padding-left: 50px !important;
          border: 1px solid red !important;
        }

        @media only screen and (max-width: 500px) {
          .logoImg {
            width: 120px;
          }
        }

      `}</style>
    </>
  );
}

export default Nawigacja;