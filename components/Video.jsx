import React from 'react'
import ReactPlayer from 'react-player/youtube'
import { Container, Row, Col } from 'reactstrap';



const Video = () => {
    return (
        <>
            <Container>
                <Row>
                    <Col>
                        <video className="video" controls="controls" loop="loop" poster="./poster.jpg" preload="none">
                            <source src="./masha.mp4" />
                        </video>
                    </Col>
                </Row>
            </Container>

            <style jsx>{`

                .video {
                    width: 100%; 
                    height: 700px;
                    text-decoration: none;
                    outline: none;
                }

                @media only screen and (max-width: 992px) {
                    .video { 
                        height: 420px;
                    }
                }

                @media only screen and (max-width: 768px) {
                    .video { 
                        height: 350px;
                    }
                }

                @media only screen and (max-width: 600px) {
                    .video { 
                        height: 320px;
                    }
                }

                @media only screen and (max-width: 400px) {
                    .video { 
                        height: 190px;
                    }
                }

                @media only screen and (max-width: 350px) {
                    .video { 
                        height: 180px;
                    }
                }
                

            `}</style>
        </>
    )
}

export default Video