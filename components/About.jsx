import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';

import 'bootstrap/dist/css/bootstrap.css';
import { Container, Row, Col } from 'reactstrap';
import Slid from './slider/Slid';



const About = () => {

  return (
    <>
        <Container>
            <Row>
                <Col>
                    <div>
                        <div id="section2">
                            <h1 className="aboutTitle">O nas</h1>
                        </div>
                    </div>
                </Col>
            </Row>
        </Container>
        
        <Container>
            <Row>
                <div className="aboutContent">
                    <Col xs={12} sm={12} lg={6}>
                        <p>
                            Sklep O - Store prowadzi sprzedaż stylowej bielizny damskiej
                            w bardzo przystępnych cenach. Dążymy do tego, żeby każda kobieta
                            w Polsce mogła upiększyć swoje ciało naszą bielizną.                
                        </p>
                        <p>
                            Pamiętaj, że to co drogie nie zawsze jest najlepsze! W dzisiejszych czasach
                            często przepłacamy nie za jakość samego produktu czy jego wykonanie a 
                            za nazwę danej marki. Z tego powodu na rynku pojawia się sklep 
                            O - Store który łączy jakoś produktu - wygode - komfort - styl - 
                            i cenę.
                        </p>
                        <p>
                            Zastanawiasz się co kupić swojej ukochanej na prezent ? Mamy rozwiązanie
                            dla ciebie! Każda kobieta mająca tysiąc przerużnej bielizny zawsze
                            będzie szczęśliwa otrzymać kolejne tysiąc aby odświerzyć swój "arsenal"
                            w swojej szafie.
                        </p>
                    </Col>
                    <Col xs={12} sm={12} lg={6}>
                        <p>
                            Szafa kobiety jest jej szansą na wyróżnienie się i zrobienie 
                            trwałego pierwszego wrażenia.
                            Nasze produkty są starannie dobrane, aby zapewnić naszym klientom najnowsze trendy.
                        </p>
                        <p>
                            O - Store to najlepsze miejsce dla fashionistek i fashionistów do 
                            utworzenia pięknej garderoby z klasą. 
                            Naszym priorytetem jest doskonała 
                            obsługa klienta, wyjątkowo szybka realizacja zamówień, bardzo 
                            szybki czas wysyłki i bezproblemowa polityka zwrotu.
                            Staramy się, aby każdy zakup 
                            był pozytywnym doświadczeniem.
                        </p>
                        <p>
                            W ofercie posiadamy: majtki figi/stringi, body i seksowne ubranka.
                        </p>
                        <p>
                             
                            Cenimy Twoją opinię, zarówno pozytywną, jak i negatywną i nieustannie 
                            pracujemy nad jak najwyższą jakością naszych produktów. 
                        </p>
                    </Col>
                </div>

                {/* Mobile */}
                <div className="aboutContent2">
                    <Col xs={12} sm={12} lg={6}>
                        <p>
                            Sklep O - Store prowadzi sprzedaż stylowej bielizny damskiej
                            w bardzo przystępnych cenach. Dążymy do tego, żeby każda kobieta
                            w Polsce mogła upiększyć swoje ciało naszą bielizną.                
                        </p>
                        <p>
                            Pamiętaj, że to co drogie nie zawsze jest najlepsze! W dzisiejszych czasach
                            często przepłacamy nie za jakość samego produktu czy jego wykonanie a 
                            za nazwę danej marki. Z tego powodu na rynku pojawia się sklep 
                            O - Store który łączy jakoś produktu - wygode - komfort - styl - 
                            i cenę.
                        </p>
                        <p>
                            Zastanawiasz się co kupić swojej ukochanej na prezent ? Mamy rozwiązanie
                            dla ciebie! Każda kobieta mająca tysiąc przerużnej bielizny zawsze
                            będzie szczęśliwa otrzymać kolejne tysiąc aby odświerzyć swój "arsenal"
                            w swojej szafie.
                        </p>

                        <br />

                        <p>
                            Szafa kobiety jest jej szansą na wyróżnienie się i zrobienie 
                            trwałego pierwszego wrażenia.
                            Nasze produkty są starannie dobrane, aby zapewnić naszym klientom najnowsze trendy.
                        </p>
                        <p>
                            O - Store to najlepsze miejsce dla fashionistek do 
                            utworzenia pięknej garderoby z klasą. 
                            Naszym priorytetem jest doskonała 
                            obsługa klienta, wyjątkowo szybka realizacja zamówień, bardzo 
                            szybki czas wysyłki i bezproblemowa polityka zwrotu.
                            Staramy się, aby każdy zakup 
                            był pozytywnym doświadczeniem.
                        </p>
                        <p>
                            W ofercie posiadamy: majtki figi/stringi, body i seksowne ubranka.
                        </p>
                        <p>
                             
                            Cenimy Twoją opinię, zarówno pozytywną, jak i negatywną i nieustannie 
                            pracujemy nad jak najwyższą jakością naszych produktów. 
                        </p>
                    </Col>
                    {/* <Col xs={12} sm={12} lg={6}>
                        <img className="aboutUsImg" src="./aboutUs.png" />
                    </Col> */}
                </div>  
            </Row>
            <Slid />
        </Container>                         
                    
            
        <style jsx>{`

            p {
                font-weight: 100;
            }

            .titleMainImg {
                position: absolute;
                left: 0;
                top: 40%;
                width: 100%;
                text-align: center;
                color: white;
                text-shadow: 2px 2px 4px black, 0 0 1em #333;
                font-size: 70px;
            }

            .titleMainImg2 {
                position: absolute;
                left: 0;
                top: 55%;
                width: 100%;
                text-align: center;
                color: white;
                text-shadow: 2px 4px 4px black, 0 0 1em #333;
            }

            .rimmaDonna {
                width: 100%;
                height: 100vh;
                border: 1px solid red;
            }
          
            .aboutTitle {
                display: flex;
                justify-content: center;
                margin-top: 150px;
                margin-bottom: 50px;
                color: rgba(255, 0, 76, 0.822);
            }

            .aboutContent {
                display: flex; 
                justify-content: center;
                align-items: center;
                align-content: center;
                margin-top: 50px;
                margin-bottom: 50px;
                // text-align: center;    
            }

            .aboutContent2 {
                display: none;
            }

            .aboutUsImg {
                width: 100%;
                background-size: cover;
                overflow: hidden;
                border-radius: 10px;
            }

            @media only screen and (max-width: 992px) {
                .aboutContent {
                    display: none;
                }
                
                .aboutContent2 {
                    display: block;
                }
            }

            @media only screen and (max-width: 992px) {
                .titleMainImg {
                    font-size: 30px;
                }

                .titleMainImg2 {
                    font-size: 18px;
                    top: 50%;
                }
            }

            @media only screen and (max-width: 500px) {
                .aboutTitle {
                    margin-top: 75px;
                }
            }

        `}</style>
    </>
  );
}

export default About;