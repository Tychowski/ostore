import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import Gallery from 'react-grid-gallery';
import Router from "next/router";
import styled from "@emotion/styled";

const MessageBottom = styled.div`
    display: flex;
    justify-content: center;
    align-content: center;
    border-radius: none;
`;


const IMAGES =
    [
        {
            src: "/gallery/1.1.jpg",
            thumbnail: "/gallery/1.1.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/1.2.jpg",
            thumbnail: "/gallery/1.2.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/1.3.jpg",
            thumbnail: "/gallery/1.3.jpg",
            thumbnailWidth: 250,
            thumbnailHeight: 200
        },
        {
            src: "/gallery/1.4.jpg",
            thumbnail: "/gallery/1.4.jpg",
            thumbnailWidth: 250,
            thumbnailHeight: 200
        },
        {
            src: "/gallery/1.5.jpg",
            thumbnail: "/gallery/1.5.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/1.6.jpg",
            thumbnail: "/gallery/1.6.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/1.7.jpg",
            thumbnail: "/gallery/1.7.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/1.8.jpg",
            thumbnail: "/gallery/1.8.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/1.9.jpg",
            thumbnail: "/gallery/1.9.jpg",
            thumbnailWidth: 250,
            thumbnailHeight: 200
        },
        {
            src: "./gallery/1.10.jpg",
            thumbnail: "./gallery/1.10.jpg",
            thumbnailWidth: 300,
            thumbnailHeight: 200
        },
        {
            src: "/gallery/1.11.jpg",
            thumbnail: "/gallery/1.11.jpg",
            thumbnailWidth: 250,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/1.12.jpg",
            thumbnail: "/gallery/1.12.jpg",
            thumbnailWidth: 300,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/1.13.jpg",
            thumbnail: "/gallery/1.13.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/2.1.jpg",
            thumbnail: "/gallery/2.1.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/2.2.jpg",
            thumbnail: "/gallery/2.2.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/2.3.jpg",
            thumbnail: "/gallery/2.3.jpg",
            thumbnailWidth: 300,
            thumbnailHeight: 200
        },
        {
            src: "/gallery/2.4.jpg",
            thumbnail: "/gallery/2.4.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/2.5.jpg",
            thumbnail: "/gallery/2.5.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/2.6.jpg",
            thumbnail: "/gallery/2.6.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/2.7.jpg",
            thumbnail: "/gallery/2.7.jpg",
            thumbnailWidth: 300,
            thumbnailHeight: 200
        }
    ]



const Galery = () => {

  return (
    <>
        <Container>
            <Row>
                <Col>
                    <div className="contactTitle">
                        <h1>Galeria</h1>
                    </div>
                </Col>
            </Row>

            <Row>
                <Col>
                <Gallery 
                    images={IMAGES} 
                    enableLightbox={true}
                    backdropClosesModal
                />
                </Col>
            </Row>

            <Row>
                <Col>
                    <div className="btnBack">
                        <div className="btn">
                            <MessageBottom onClick={() => Router.push("/galery")}>Zobacz wiecej</MessageBottom>
                        </div>
                    </div>
                </Col>
            </Row>
        </Container>
                                            
            
        <style jsx>{`

            .contactTitle {
                display: flex;
                justify-content: center;
                margin-top: 50px;
                margin-bottom: 50px;
                color: rgba(255, 0, 76, 0.822);
            }

            .btnBack {
                display: flex;
                justify-content: center;
                align-content: center;
                margin: 20px 0px 100px;
            }

            .btn {
                padding: 15px 25px;
                font-weight: 200;
                cursor: pointer;
                width: 200px;
                background: #93d1b7;
                transition: all 0.4s ease-in-out;
            }

            .btn:hover {
                background: #63f3b7;
                transition: all 1s ease-in-out;
            }
            

            @media only screen and (max-width: 550px) {
                
            }

        `}</style>
    </>
  );
}

export default Galery;