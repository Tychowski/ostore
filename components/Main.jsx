import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';

import 'bootstrap/dist/css/bootstrap.css';
import { Container, Row, Col } from 'reactstrap';
import MainImg from "./prebuilt/img/MainImg";
import MainImgSmall from './prebuilt/img/MainImgSmall';
import MainImgSmall2 from './prebuilt/img/MainImgSmall2';
import MainImgSmall3 from './prebuilt/img/MainImgSmall3';
import ImgIpad from './prebuilt/img/ImgIpad';
import ImgIpad2 from './prebuilt/img/ImgIpad2';
import ImgMob from './prebuilt/img/ImgMob';
import ImgMob2 from './prebuilt/img/ImgMob2';
import ImgMob3 from './prebuilt/img/ImgMob3';





const Main = () => {

  return (
    <>
        <div>
            <MainImg src="./main/1920x1080.jpeg" />
            <h1 className="titleMainImg">
                <img src="./ostoreHead.png" width="200"/>
            </h1>
            <h5 className="titleMainImg2">Damska bielizna najlepszej jakości</h5>
        </div>
        
        <MainImgSmall src="./main/1440x900.jpeg" />
        <MainImgSmall2 src="./main/1366x768.jpeg" />
        <MainImgSmall3 src="./main/1280x800.jpeg" />

        {/* Tablet */}
        <ImgIpad src="./main/masza-md.jpeg" />
        <ImgIpad2 src="./main/masza-md2.jpeg" />


        {/* Mobile */}
        <ImgMob src="./main/masza-xs.jpeg" />
        <ImgMob2 src="./main/masza-xs2.jpeg" />
        <ImgMob3 src="./main/masza-xs3.jpeg" />
        

        
                    
            
        <style jsx>{`

            .titleMainImg {
                position: absolute;
                left: 0;
                top: 40%;
                width: 100%;
                text-align: center;
                color: white;
                text-shadow: 2px 2px 4px black, 0 0 1em #333;
                font-size: 70px;
            }

            .titleMainImg2 {
                position: absolute;
                left: 0;
                top: 55%;
                width: 100%;
                text-align: center;
                color: white;
                text-shadow: 2px 4px 4px black, 0 0 1em #333;
                font-weight: 100;
            }

            @media only screen and (max-width: 992px) {
                .titleMainImg {
                    font-size: 30px;
                }

                .titleMainImg2 {
                    font-size: 18px;
                    top: 50%;
                }
            }

            @media only screen and (max-width: 500px) {
                .titleMainImg {
                    font-size: 50px;
                    top: 50%;
                }

                .titleMainImg2 {
                    top: 60%;
                }
            }

            @media only screen and (max-width: 400px) {
                .titleMainImg {
                    font-size: 50px;
                    top: 50%;
                }

                .titleMainImg2 {
                    top: 63%;
                }
            }

            @media only screen and (max-width: 350px) {
                .titleMainImg {
                    font-size: 50px;
                    top: 65%;
                }

                .titleMainImg2 {
                    top: 80%;
                }
            }

            
            

        `}</style>
    </>
  );
}

export default Main;