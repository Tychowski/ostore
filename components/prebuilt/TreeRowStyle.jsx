import styled from "@emotion/styled";

const TreeRowStyle = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-around;
    background-color: rgb(255, 241, 245);
    width: 100%;
`;

export default TreeRowStyle;

