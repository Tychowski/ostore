import styled from "@emotion/styled";

const Discount = styled.span`
    text-align: center;
    height: 20px;
    color: #fff;
    font-weight: 200;
    font-size: 16px;
    text-decoration: line-through;
    padding-top: 20px;



    display: flex;
	flex-direction: RowStyle;
	flex-wrap: wrap;
	justify-content: center;
	align-items: center;
	align-content: center;
`;

export default Discount;

