import { Global, css } from "@emotion/core";

const GlobalStyles = () => (
  <>
    <Global
      styles={css`
        *,
        *:before,
        *:after {
          box-sizing: border-box;
        }
      `}
    />
    <Global
      styles={css`
        input,
        button {
          -webkit-appearance: none;
          -moz-appearance: none;
          appearance: none;
          outline: none;
          border-style: none;
        }
      `}
    />
    <Global
      styles={css`
        body,
        html {
          background-color: rgb(255, 241, 245);
          font-size: 18px;
          font-family: 'Roboto', sans-serif;
        }
      `}
    />
    <Global
      styles={css`
        h1, h2, h3, h4, h5, h6 {
          font-weight: 100;
        }
      `}
    />
  </>
);

export default GlobalStyles;
