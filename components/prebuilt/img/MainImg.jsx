import styled from "@emotion/styled";

const MainImg = styled.img`
  display: block;
  margin: 0 auto;
  border-radius: 10px;
  background-size: cover;
  object-fit: cover;

  width: 100%;
  background-size: cover;
  overflow: hidden;
  box-shadow: 0 6px 9px rgba(50, 50, 93, 0.06), 0 2px 5px rgba(0, 0, 0, 0.08),
    inset 0 1px 0 #d1d1d1;
  border-radius: 4px;


  @media only screen and (max-width: 1440px) {
    display: none;
  }

`;

export default MainImg;
