import styled from "@emotion/styled";

const MainIpad2 = styled.img`
  display: none;
  margin: 0 auto;
  border-radius: 10px;
  background-size: cover;

  width: 100%;
  // height: 94vh;
  box-shadow: 0 6px 9px rgba(50, 50, 93, 0.06), 0 2px 5px rgba(0, 0, 0, 0.08),
    inset 0 1px 0 #d1d1d1;
  border-radius: 4px;

  @media (max-width: 677px) and (min-width: 501px) {
    display: block;
  }

`;

export default MainIpad2;

