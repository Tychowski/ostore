import FormField from "./FormField";

const BillingDetailsFields = (title) => {
  return (
    <>
      <FormField
        name="name"
        label="Imię/Nazwisko"
        type="text"
        placeholder="Jan Kowalski"
        required
      />
      <FormField
        name="size"
        label="Rozmiar"
        type="text"
        placeholder="S-M-L"
        required
      />
      {/* wzrost: 165cm, rękaw: 55cm, talia: 60cm */}
      <FormField
        name="email"
        label="Email"
        type="email"
        placeholder="kowalski@example.com"
        required
      />
      <FormField
        name="address"
        label="Adres"
        type="text"
        placeholder="ul. Studencka 17"
        required
      />
      <FormField
        name="city"
        label="Miasto"
        type="text"
        placeholder="Warszawa"
        required
      />
      <FormField
        name="zip"
        label="Kod pocztowy"
        type="text"
        placeholder="31-001"
        required
      />
      <FormField
        name="dostawa"
        label="Dostawa"
        type="disabled"
        placeholder="InPost/List polecony"
        disabled={true} 
      />
    </>
  );
};

export default BillingDetailsFields;
