import styled from "@emotion/styled";

const RowStyleWithoutBg = styled.div`
  max-width: 400px;
  margin: 30px auto;
  border-radius: 4px;
  position: relative;
`;

export default RowStyleWithoutBg;
