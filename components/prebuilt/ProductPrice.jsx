import styled from "@emotion/styled";

const ProductPrice = styled.span`
    text-align: center;
    height: 20px;
    color: #fff;
    font-weight: 300;
    font-size: 22px;
    margin-top: 10px;

    display: flex;
	flex-direction: RowStyle;
	flex-wrap: wrap;
	justify-content: center;
	align-items: center;
	align-content: center;
`;

export default ProductPrice;

