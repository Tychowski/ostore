import styled from "@emotion/styled";

const SubmitButton = styled.button`
position: relative;
left: 50%;
transform: translate(-50%, 0);

  height: 40px;
  width: 100%;
  font-size: inherit;
  background-color: ${props => (props.disabled ? "rgba(146, 146, 146, 0.486)" : "rgb(82, 82, 82)")};
  box-shadow: ${props =>
    props.disabled
      ? "none"
      : "0 6px 9px rgba(50, 50, 93, 0.06), 0 2px 5px rgba(0, 0, 0, 0.08), inset 0 1px 4px 0 #e2e2e2;"};
  border-radius: 4px;
  opacity: ${props => (props.disabled ? 0.5 : 1)};
  color: #fff;
  font-weight: 200;
  cursor: pointer;
`;

export default SubmitButton;
