import styled from "@emotion/styled";

const Image = styled.img`
  display: block;
  margin: 0 auto;
  border-radius: 10px;

  width: 280px;
  box-shadow: 0 6px 9px rgba(50, 50, 93, 0.06), 0 2px 5px rgba(0, 0, 0, 0.08),
    inset 0 1px 0 #d1d1d1;
  border-radius: 4px;
  background-color: rgb(189, 189, 189);
  position: relative;

  @media only screen and (max-width: 1550px) {
    width: 300px;
  }

  @media only screen and (max-width: 1250px) {
    width: 235px;
  }

  @media only screen and (max-width: 992px) {
    width: 305px;
  }

  @media only screen and (max-width: 910px) {
    width: 290px;
  }

  @media only screen and (max-width: 780px) {
    width: 290px;
  }

  @media only screen and (max-width: 680px) {
    width: 280px;
  }

  @media only screen and (max-width: 450px) {
    width: 330px;
  }

  @media only screen and (max-width: 430px) {
    width: 300px;
  }

  @media only screen and (max-width: 400px) {
    width: 290px;
  }

  @media only screen and (max-width: 380px) {
    width: 265px;
  }

  @media only screen and (max-width: 355px) {
    width: 250px;
  }

  @media only screen and (max-width: 335px) {
    width: 235px;
  }

  @media only screen and (max-width: 310px) {
    width: 200px;
  }
`;

export default Image;
