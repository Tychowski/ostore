import styled from "@emotion/styled";

const BuyClose = styled.button`
  position: relative;
  left: 50%;
  transform: translate(-50%, 0);
  text-align: center;
  border-radius: 4px;
  border: none;

  height: 40px;
  width: 150px;
  font-size: 16px;
  background-color: rgb(82, 82, 82);
  box-shadow: ${props =>
    props.disabled
      ? "none"
      : "0 6px 9px rgba(50, 50, 93, 0.06), 0 4px 7px rgba(0, 0, 0, 0.08), inset 0 1px 4px 0 #e2e2e2;"};
  
  opacity: ${props => (props.disabled ? 0.5 : 1)};
  color: #fff;
  font-weight: 200;
  cursor: pointer;
`;

export default BuyClose;

