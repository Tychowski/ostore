import styled from "@emotion/styled";

const ProductSize = styled.span`
    text-align: center;
    height: 20px;
    color: #fff;
    font-weight: 200;
    margin-top: 10px;
    padding-top: 20px;


    display: flex;
	flex-direction: RowStyle;
	flex-wrap: wrap;
	justify-content: center;
	align-items: center;
	align-content: center;
`;

export default ProductSize;

