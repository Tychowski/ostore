import styled from "@emotion/styled";

import { useState } from "react";
import Image from "./Image";
import DonutQuantity from "./Quantity";
import RowStyleWithoutBg from "./RowStyleWithoutBg";
import ProductTitle from "./ProductTitle";
import ProductPrice from "./ProductPrice";
import ProductSize from "./ProductSize";
import 'bootstrap/dist/css/bootstrap.min.css';
import Discount from "./Discount";

const Shop = styled.div`
  padding: 10px 20px 40px 20px;
  border-top: none !important;
  background: rgb(255, 178, 197);  
  border-radius: 5px;
`;

const ShopName = styled.h1`
  color: #fff;
  font-style: normal;
  font-variant: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 26.4px;
  margin-bottom: 20px;
`;

const Info = styled.span`
  position: absolute;
  right: 0;
  font-size: 16px;
  color: #fff;
  padding: 2px 20px;
  margin: 0px 5px;
  border-radius: 7px;
  font-weight: 300;
  line-height: 26.4px;
  margin-bottom: 20px;
`;

const Controls = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  margin-top: 40px;
`;

const WomanShop = ({ onAddDonut, onRemoveDonut, numDonuts, titleProduct, priceProduct, pathImage, discount, info, sell }) => {


  return (
    <>
    <Shop>
      <Info>
        <div className="sell">
          {sell}
        </div>  
      </Info>
      <Info>
        <div className="discountPrice">
          {info}
        </div>  
      </Info>
      <ShopName>O - Store</ShopName>
      <Image src={pathImage}></Image>
      <RowStyleWithoutBg>
        <ProductTitle>
          {titleProduct}
        </ProductTitle>
        <Discount>
          {discount}
        </Discount>
        <ProductPrice>
          {priceProduct}
        </ProductPrice>
      </RowStyleWithoutBg>
      <Controls>
        <DonutQuantity
          onAdd={onAddDonut}
          onRemove={onRemoveDonut}
          quantity={numDonuts}
        />
      </Controls>
    </Shop>
    <style jsx>{`

      .sell {
        border-radius: 5px;
        padding: 0px 10px;
        background: rgb(51, 145, 221);
      }

      .discountPrice {
        border-radius: 5px;
        padding: 0px 10px;
        background: rgba(255, 0, 76, 0.822);
      }

      .bg-red {
        background: rgb(255, 127, 165);

        text-align: center;
        color: #333;
        font-weight: 400;
        font-size: 15px;
        margin: 5px 15px;
        padding: 0px 8px;
        border: 1px solid #333;
        cursor: pointer;
      } 
      
      .bg-blue {
        background: #D4D4D4;

        text-align: center;
        color: #333;
        font-weight: 400;
        font-size: 15px;
        margin: 5px 15px;
        padding: 0px 8px;
        border: 1px solid #333;
        cursor: pointer;
      }

    `}</style>
    </>
  );
};

export default WomanShop;
