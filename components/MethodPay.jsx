import React from 'react';
import { Container, Row, Col } from 'reactstrap';


const MethodPay = () => {

  return (
    <>
        <Container>
            <Row>
                <Col>
                    <div className="contactTitle">
                        <h1 className="titl">Z nami bezpiecznie</h1>
                    </div>
                    <div className="contentContact">
                        <div className="payShip">PLATNOŚĆ / WYSYŁKA</div>
                        <div className="sectionPhone">
                            <img src="./icon.ico/visa.svg" width="100" />
                            <img src="./icon.ico/master.svg" width="120" />
                        </div>
                        <div>
                            <img src="./icon.ico/inPost.svg" width="100" />
                            <img src="./icon.ico/pacz23.png" width="100" />
                        </div>
                    </div>
                </Col>
            </Row>
        </Container>
                                            
            
        <style jsx>{`

            .contactTitle {
                display: flex;
                justify-content: center;
                text-align: center;
                margin: 100px 0px 50px;
                color: rgba(255, 0, 76, 0.822);
            }

            .infoContact {
                display: flex;
                justify-content: center;
                text-align: center;
            }

            .payShip {
                font-weight: 100;
                margin: 35px;
            }

            .contentContact {
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                justify-content: space-around;
                align-items: center;
                align-content: center;

                margin: 50px 0px;
                text-align: center;
            }

            .phone {
                font-weight: 100;
            }

            .iconPhone {
                width: 40px;
                margin: 10px;
            }

            .smallTitle {
                margin-bottom: 20px;
                font-weight: 100;
            }

            @media only screen and (max-width: 600px) {

                .sectionPhone {
                    margin-bottom: 60px;
                }
                
                .contentContact {
                    display: inline-block;
                    width: 100%;
                }

                .payShip {
                    margin-bottom: 135px !import;
                }
            }

        `}</style>
    </>
  );
}

export default MethodPay;