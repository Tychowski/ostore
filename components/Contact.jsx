import React from 'react';
import { Container, Row, Col } from 'reactstrap';


const Contact = () => {

  return (
    <>
        <Container>
            <Row>
                <Col>
                    <div className="contactTitle">
                        <h1>Kontakt</h1>
                    </div>
                    <div className="infoContact">
                        <h6>
                            Jeżeli posiadasz pytania śmiało możesz napisać do nas na 
                            instagrama, email lub zadzwonić pod numer telefonu. Naszi konsultanci 
                            zaopiekują się tobą i pomogą w dobraniu najbardziej pasującej bielizny.
                        </h6>
                    </div>
                    <div className="contentContact">
                        <div className="sectionPhone">
                            <h4 className="smallTitle">Telefon</h4>
                            <a href="tel:+48733409973">
                                <img className="iconPhone" src="./phone.png" />
                                <span className="phone">+48 733 409 973</span>
                            </a>
                            <br />
                            {/* <a href="tel:+48884927220">
                                <img className="iconPhone" src="./phone.png" />
                                <span className="phone mar">+48 821 127 340</span>
                            </a> */}
                        </div>
                        <div>
                            <h4 className="smallTitle">E-mail</h4>
                            <a href="mailto:ostore.polska@gmail.com">
                                <img className="iconPhone" src="./gmail.png" />
                                <span className="phone">ostore.polska@gmail.com</span>
                            </a>
                            <br />
                            {/* <a href="tel:+48884927220">
                                <img className="iconPhone" src="./gmail.png" />
                                <span className="phone">exapmle@gmail.com</span>
                            </a> */}
                        </div>
                    </div>
                </Col>
            </Row>
        </Container>
                                            
            
        <style jsx>{`

            .contactTitle {
                display: flex;
                justify-content: center;
                margin: 100px 0px 50px;
                color: rgba(255, 0, 76, 0.822);
            }

            .infoContact {
                display: flex;
                justify-content: center;
                text-align: center;
            }

            .contentContact {
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                justify-content: space-around;
                align-items: center;
                align-content: center;

                margin: 50px 0px;
                text-align: center;
            }

            .phone {
                font-weight: 100;
            }

            .iconPhone {
                width: 40px;
                margin: 10px;
            }

            .smallTitle {
                margin-bottom: 20px;
                font-weight: 100;
            }

            @media only screen and (max-width: 600px) {

                .sectionPhone {
                    margin-bottom: 60px;
                }
                
                .contentContact {
                    display: inline-block;
                    width: 100%;
                }
            }

        `}</style>
    </>
  );
}

export default Contact;