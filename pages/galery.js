import styled from "@emotion/styled";
import { useState, useEffect } from "react";
import Router from "next/router";
import { Container, Row, Col } from 'reactstrap';
import Gallery from 'react-grid-gallery';
import 'bootstrap/dist/css/bootstrap.css';
import Footer from "../components/Footer";

import Layout from "../components/Layout";
import NawigacjaGalery from "../components/NawigacjaGalery";


const MessageBottom = styled.div`
    display: flex;
    justify-content: center;
    align-content: center;
    border-radius: none;
`;

const IMAGES =
    [
        {
            src: "/gallery/1.1.jpg",
            thumbnail: "/gallery/1.1.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/1.2.jpg",
            thumbnail: "/gallery/1.2.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/1.3.jpg",
            thumbnail: "/gallery/1.3.jpg",
            thumbnailWidth: 250,
            thumbnailHeight: 200
        },
        {
            src: "/gallery/1.4.jpg",
            thumbnail: "/gallery/1.4.jpg",
            thumbnailWidth: 250,
            thumbnailHeight: 200
        },
        {
            src: "/gallery/1.5.jpg",
            thumbnail: "/gallery/1.5.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/1.6.jpg",
            thumbnail: "/gallery/1.6.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/1.7.jpg",
            thumbnail: "/gallery/1.7.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/1.8.jpg",
            thumbnail: "/gallery/1.8.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/1.9.jpg",
            thumbnail: "/gallery/1.9.jpg",
            thumbnailWidth: 250,
            thumbnailHeight: 200
        },
        {
            src: "./gallery/1.10.jpg",
            thumbnail: "./gallery/1.10.jpg",
            thumbnailWidth: 300,
            thumbnailHeight: 200
        },
        {
            src: "/gallery/1.11.jpg",
            thumbnail: "/gallery/1.11.jpg",
            thumbnailWidth: 250,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/1.12.jpg",
            thumbnail: "/gallery/1.12.jpg",
            thumbnailWidth: 300,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/1.13.jpg",
            thumbnail: "/gallery/1.13.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/2.1.jpg",
            thumbnail: "/gallery/2.1.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/2.2.jpg",
            thumbnail: "/gallery/2.2.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/2.3.jpg",
            thumbnail: "/gallery/2.3.jpg",
            thumbnailWidth: 300,
            thumbnailHeight: 200
        },
        {
            src: "/gallery/2.4.jpg",
            thumbnail: "/gallery/2.4.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/2.5.jpg",
            thumbnail: "/gallery/2.5.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/2.6.jpg",
            thumbnail: "/gallery/2.6.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/2.7.jpg",
            thumbnail: "/gallery/2.7.jpg",
            thumbnailWidth: 300,
            thumbnailHeight: 200
        },
        {
            src: "/gallery/3.1.jpg",
            thumbnail: "/gallery/3.1.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/3.2.jpg",
            thumbnail: "/gallery/3.2.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/3.3.jpg",
            thumbnail: "/gallery/3.3.jpg",
            thumbnailWidth: 300,
            thumbnailHeight: 200
        },
        {
            src: "/gallery/3.4.jpg",
            thumbnail: "/gallery/3.4.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/3.5.jpg",
            thumbnail: "/gallery/3.5.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/3.6.jpg",
            thumbnail: "/gallery/3.6.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/3.7.jpg",
            thumbnail: "/gallery/3.7.jpg",
            thumbnailWidth: 250,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/3.8.jpg",
            thumbnail: "/gallery/3.8.jpg",
            thumbnailWidth: 300,
            thumbnailHeight: 200
        },
        {
            src: "/gallery/3.9.jpg",
            thumbnail: "/gallery/3.9.jpg",
            thumbnailWidth: 300,
            thumbnailHeight: 200
        },
        {
            src: "/gallery/3.10.jpg",
            thumbnail: "/gallery/3.10.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/3.11.jpg",
            thumbnail: "/gallery/3.11.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/3.12.jpg",
            thumbnail: "/gallery/3.12.jpg",
            thumbnailWidth: 300,
            thumbnailHeight: 200
        },
        {
            src: "/gallery/4.1.jpg",
            thumbnail: "/gallery/4.1.jpg",
            thumbnailWidth: 300,
            thumbnailHeight: 200
        },
        {
            src: "/gallery/4.2.jpg",
            thumbnail: "/gallery/4.2.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/4.3.jpg",
            thumbnail: "/gallery/4.3.jpg",
            thumbnailWidth: 250,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/4.4.jpg",
            thumbnail: "/gallery/4.4.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/4.5.jpg",
            thumbnail: "/gallery/4.5.jpg",
            thumbnailWidth: 300,
            thumbnailHeight: 200
        },
        {
            src: "/gallery/4.6.jpg",
            thumbnail: "/gallery/4.6.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/4.7.jpg",
            thumbnail: "/gallery/4.7.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/4.8.jpg",
            thumbnail: "/gallery/4.8.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/4.9.jpg",
            thumbnail: "/gallery/4.9.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/4.10.jpg",
            thumbnail: "/gallery/4.10.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/4.11.jpg",
            thumbnail: "/gallery/4.11.jpg",
            thumbnailWidth: 250,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/4.12.jpg",
            thumbnail: "/gallery/4.12.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/4.13.jpg",
            thumbnail: "/gallery/4.13.jpg",
            thumbnailWidth: 300,
            thumbnailHeight: 200
        },
        {
            src: "/gallery/4.14.jpg",
            thumbnail: "/gallery/4.14.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/4.15.jpg",
            thumbnail: "/gallery/4.15.jpg",
            thumbnailWidth: 250,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/4.16.jpg",
            thumbnail: "/gallery/4.16.jpg",
            thumbnailWidth: 250,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/4.17.jpg",
            thumbnail: "/gallery/4.17.jpg",
            thumbnailWidth: 300,
            thumbnailHeight: 200
        },
        {
            src: "/gallery/4.18.jpg",
            thumbnail: "/gallery/4.18.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/4.19.jpg",
            thumbnail: "/gallery/4.19.jpg",
            thumbnailWidth: 300,
            thumbnailHeight: 200
        },
        {
            src: "/gallery/4.20.jpg",
            thumbnail: "/gallery/4.20.jpg",
            thumbnailWidth: 250,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/4.21.jpg",
            thumbnail: "/gallery/4.21.jpg",
            thumbnailWidth: 250,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/4.22.jpg",
            thumbnail: "/gallery/4.22.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/4.23.jpg",
            thumbnail: "/gallery/4.23.jpg",
            thumbnailWidth: 300,
            thumbnailHeight: 200
        },
        {
            src: "/gallery/4.24.jpg",
            thumbnail: "/gallery/4.24.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/4.25.jpg",
            thumbnail: "/gallery/4.25.jpg",
            thumbnailWidth: 200,
            thumbnailHeight: 250
        },
        {
            src: "/gallery/4.26.jpg",
            thumbnail: "/gallery/4.26.jpg",
            thumbnailWidth: 300,
            thumbnailHeight: 200
        }
    ]


export default () => {

  return (
    <>
    <NawigacjaGalery />
    <Layout title="galeria">
    <Container>
        <Row>
            <Col>
                <div>
                    <h1 className="title">Galeria</h1>
                </div>
                <Gallery 
                    images={IMAGES} 
                    enableLightbox={true}
                    backdropClosesModal
                    id="gale"
                />
            </Col>
        </Row>
    </Container>

    <div className="btnBack">
        <div className="btn">
            <MessageBottom onClick={() => Router.push("/")}>Główna</MessageBottom>
        </div>
    </div>
    <Footer />


        <style jsx>{`


            .title {
                display: flex;
                justify-content: center;
                align-content: center;
                margin: 150px 0px 20px;
                color: rgba(255, 0, 76, 0.822);
            }

            .btnBack {
                display: flex;
                justify-content: center;
                align-content: center;
                margin: 20px 0px 150px;
            }

            .btn {
                padding: 15px 25px;
                font-weight: 200;
                cursor: pointer;
                width: 200px;
                background: #93d1b7;
                transition: all 0.4s ease-in-out;
            }

            .btn:hover {
                background: #63f3b7;
                transition: all 1s ease-in-out;
            }

            
            

            @media only screen and (max-width: 550px) {
                
            }

        `}</style>
    </Layout>
    </>
  );
};
