import styled from "@emotion/styled";
import Confetti from "react-confetti";
import { useState, useEffect } from "react";
import Router from "next/router";

import Layout from "../components/Layout";
import RowStyle from "../components/prebuilt/RowStyle";
import BuyClose from "../components/prebuilt/BuyClose";

const Container = styled.div`
  width: 300px;
  margin: 30px auto 0 auto;
  text-align: center;
  color: #333;
`;

const Title = styled.div`
  font-size: 40px;
  color: rgba(0, 219, 146, 0.966);
`;

const MessageMain = styled.div`
  margin-top: 40px;
  font-size: 20px;
`;

const Message = styled.div`
  margin-top: 40px;
  font-size: 14px;
`;

const MessageBottom = styled.div`
  margin-top: 30%;
  font-size: 16px;
  border: 1px solid #333;
  font-weight: 300;
  padding: 20px 0px;
  cursor: pointer;
  background-color: rgb(255, 161, 211);
`;


export default () => {
  const [width, setWidth] = useState(0);
  const [height, setHeight] = useState(0);

  useEffect(() => {
    setTimeout(() => {
      setWidth(window.innerWidth);
      setHeight(window.innerHeight);
    }, 100);
  });

  return (
    <Layout title="Success!">
      <Container>
        <Confetti width={width} height={height} numberOfPieces={450} />
        <Title>Płatność realizowana !</Title>
        <MessageMain>Dziękujemy za wybranie naszego sklepu</MessageMain>
        {/* <Message>Każde nasze zamówienie zostaje wysłane przez InPost</Message> */}
        <MessageBottom onClick={() => Router.push("/")}>Ostore</MessageBottom>
      </Container>
    </Layout>
  );
};
