import { useState } from "react";
import Router from "next/router";

import Layout from "../components/Layout";
import RowStyle from "../components/prebuilt/RowStyle";
import WomanShop from "../components/prebuilt/WomanShop";
import CheckoutForm from "../components/CheckoutForm";
import { 
  getDonutPrice,
  getDonutPrice2,
  getDonutPrice3,
  getDonutPrice4,
  getDonutPrice5,
  getDonutPrice6,
  getDonutPrice7,
  getDonutPrice8,
  getDonutPrice9,
  getDonutPrice10,
  getDonutPrice11,
  getDonutPrice12
 } from "../utils/setPrice";
import TreeRowStyle from "../components/prebuilt/TreeRowStyle";
import styled from "@emotion/styled";
import 'bootstrap/dist/css/bootstrap.css';
import { Container, Row, Col } from 'reactstrap';
import Nawigacja from "../components/Nawigacja";
import About from "../components/About";
import Main from "../components/Main";
import Footer from "../components/Footer";
import ScrollUpButton from "react-scroll-up-button";
import Contact from "../components/Contact";
import HowToBuy from "../components/HowToBuy";
import Galery from "../components/Galery";
import Question from "../components/Question";
import Video from "../components/Video";
import RowStyleWithoutBg from "../components/prebuilt/RowStyleWithoutBg";
import DisableButton from "../components/prebuilt/DisableButton";
import MethodPay from "../components/MethodPay";


const MainPage = props => {
  const [numDonuts, setNumDonuts] = useState(1);
  const [numDonuts2, setNumDonuts2] = useState(1);
  const [numDonuts3, setNumDonuts3] = useState(1);
  const [numDonuts4, setNumDonuts4] = useState(1);
  const [numDonuts5, setNumDonuts5] = useState(1);
  const [numDonuts6, setNumDonuts6] = useState(1);
  const [numDonuts7, setNumDonuts7] = useState(1);
  const [numDonuts8, setNumDonuts8] = useState(1);
  const [numDonuts9, setNumDonuts9] = useState(1);
  const [numDonuts10, setNumDonuts10] = useState(1);
  const [numDonuts11, setNumDonuts11] = useState(1);
  const [numDonuts12, setNumDonuts12] = useState(1);
  


  const addDonut = () => setNumDonuts(num => Math.min(3, num + 1));
  const remDonut = () => setNumDonuts(num => Math.max(1, num - 1));

  const addDonut2 = () => setNumDonuts2(num => Math.min(3, num + 1));
  const remDonut2 = () => setNumDonuts2(num => Math.max(1, num - 1));

  const addDonut3 = () => setNumDonuts3(num => Math.min(3, num + 1));
  const remDonut3 = () => setNumDonuts3(num => Math.max(1, num - 1));

  const addDonut4 = () => setNumDonuts4(num => Math.min(3, num + 1));
  const remDonut4 = () => setNumDonuts4(num => Math.max(1, num - 1));

  const addDonut5 = () => setNumDonuts5(num => Math.min(3, num + 1));
  const remDonut5 = () => setNumDonuts5(num => Math.max(1, num - 1));

  const addDonut6 = () => setNumDonuts6(num => Math.min(3, num + 1));
  const remDonut6 = () => setNumDonuts6(num => Math.max(1, num - 1));

  // 
  // 
  // 
  const addDonut7 = () => setNumDonuts7(num => Math.min(3, num + 1));
  const remDonut7 = () => setNumDonuts7(num => Math.max(1, num - 1));

  const addDonut8 = () => setNumDonuts8(num => Math.min(3, num + 1));
  const remDonut8 = () => setNumDonuts8(num => Math.max(1, num - 1));

  const addDonut9 = () => setNumDonuts9(num => Math.min(3, num + 1));
  const remDonut9 = () => setNumDonuts9(num => Math.max(1, num - 1));

  const addDonut10 = () => setNumDonuts10(num => Math.min(3, num + 1));
  const remDonut10 = () => setNumDonuts10(num => Math.max(1, num - 1));

  const addDonut11 = () => setNumDonuts11(num => Math.min(3, num + 1));
  const remDonut11 = () => setNumDonuts11(num => Math.max(1, num - 1));

  const addDonut12 = () => setNumDonuts12(num => Math.min(3, num + 1));
  const remDonut12 = () => setNumDonuts12(num => Math.max(1, num - 1));



  const Divs = styled.div`
    margin: 5px;
  `;

  const SectionTitle = styled.div`
    margin: 5px;
    color: rgba(255, 0, 76, 0.822);
    font-size: 45px;
    margin-top: 100px; 
    margin-bottom: 50px;
    text-align: center;
    font-weight: 100;
`;

  return (
    <>
      <ScrollUpButton />
      <Nawigacja />
      <Main />



      <Container>
        <SectionTitle id="section1">Bielizna</SectionTitle>

        {/* Body section */}
        <Row>
          <TreeRowStyle>
            <Col xs="12" sm="12" md="12" lg="4" xl="4">
              <Divs>
                <Layout title="O - store">
                  <RowStyle>
                    <WomanShop
                      info={'Promocja'}
                      titleProduct={'Seksowne Białe Body'}
                      discount={'89.95 zł'}
                      priceProduct={'59.95 zł'}
                      pathImage={"./product/2.0.jpeg"}
                      onAddDonut={addDonut10}
                      onRemoveDonut={remDonut10}
                      numDonuts={numDonuts10}
                    />
                  </RowStyle>
                  <CheckoutForm
                    price={getDonutPrice10(numDonuts10)}
                    onSuccessfulCheckout={() => Router.push("/success")}
                  />
                </Layout>
              </Divs>
            </Col>

            <Col xs="12" sm="12" md="12" lg="4" xl="4">
              <Divs>
                <Layout title="O - store">
                  <RowStyle>
                    <WomanShop
                      sell={'Wyprzedane'}
                      titleProduct={'Erotyczny Komplet'}
                      priceProduct={'69.95 zł'}
                      pathImage={"./product/2.2.jpg"}
                      onAddDonut={addDonut11}
                      onRemoveDonut={remDonut11}
                      numDonuts={numDonuts11}
                    />
                  </RowStyle>
                  <RowStyleWithoutBg>
                    <DisableButton>
                      Kup
                    </DisableButton>
                  </RowStyleWithoutBg>
                  {/* <CheckoutForm
                    price={getDonutPrice11(numDonuts11)}
                    onSuccessfulCheckout={() => Router.push("/success")}
                  /> */}
                </Layout>
              </Divs>
            </Col>

            <Col xs="12" sm="12" md="12" lg="4" xl="4">
              <Divs>
                <Layout title="O - store">
                  <RowStyle>
                    <WomanShop
                      info={'Promocja'}
                      titleProduct={'Seksowne Czarne Body'}
                      discount={'89.95 zł'}
                      priceProduct={'59.95 zł'}
                      pathImage={"./product/2.1.jpg"}
                      onAddDonut={addDonut12}
                      onRemoveDonut={remDonut12}
                      numDonuts={numDonuts12}
                    />
                  </RowStyle>
                  <CheckoutForm
                    price={getDonutPrice12(numDonuts12)}
                    onSuccessfulCheckout={() => Router.push("/success")}
                  />
                </Layout>
              </Divs>
            </Col>
          </TreeRowStyle>
        </Row>


        <br />
        <br />
        <br />


        {/* Stringi section */}
        <Row>
          <TreeRowStyle>
            <Col xs="12" sm="12" md="12" lg="4" xl="4">
              <Divs>
                <Layout title="O - store">
                  <RowStyle>
                    <WomanShop
                      info={'Promocja'}
                      titleProduct={'Stringi Różowe'}
                      discount={'29.95 zł'}
                      priceProduct={'19.95 zł'}
                      pathImage={"./product/11.jpg"}
                      onAddDonut={addDonut}
                      onRemoveDonut={remDonut}
                      numDonuts={numDonuts}
                    />
                  </RowStyle>
                  <CheckoutForm
                    price={getDonutPrice(numDonuts)}
                    onSuccessfulCheckout={() => Router.push("/success")}
                  />
                </Layout>
              </Divs>
            </Col>

            <Col xs="12" sm="12" md="12" lg="4" xl="4">
              <Divs>
                <Layout title="O - store">
                  <RowStyle>
                    <WomanShop
                      info={'Promocja'}
                      titleProduct={'Stringi Białe'}
                      discount={'29.95 zł'}
                      priceProduct={'19.95 zł'}
                      pathImage={"./product/22.jpg"}
                      onAddDonut={addDonut2}
                      onRemoveDonut={remDonut2}
                      numDonuts={numDonuts2}
                    />
                  </RowStyle>
                  <CheckoutForm
                    price={getDonutPrice2(numDonuts2)}
                    onSuccessfulCheckout={() => Router.push("/success")}
                  />
                </Layout>
              </Divs>
            </Col>

            <Col xs="12" sm="12" md="12" lg="4" xl="4">
              <Divs>
                <Layout title="O - store">
                  <RowStyle>
                    <WomanShop
                      info={'Promocja'}
                      titleProduct={'Stringi Zielone'}
                      discount={'29.95 zł'}
                      priceProduct={'19.95 zł'}
                      pathImage={"./product/33.jpg"}
                      onAddDonut={addDonut3}
                      onRemoveDonut={remDonut3}
                      numDonuts={numDonuts3}
                    />
                  </RowStyle>
                  <CheckoutForm
                    price={getDonutPrice3(numDonuts3)}
                    onSuccessfulCheckout={() => Router.push("/success")}
                  />
                </Layout>
              </Divs>
            </Col>
          </TreeRowStyle>
        </Row>

        <br />
        <br /> 
        <br /> 

        <Row>
          <TreeRowStyle>
            <Col xs="12" sm="12" md="12" lg="4" xl="4">
              <Divs>
                <Layout title="O - store">
                  <RowStyle>
                    <WomanShop
                      info={'Promocja'}
                      titleProduct={'Stringi Czarne'}
                      discount={'29.95 zł'}
                      priceProduct={'19.95 zł'}
                      pathImage={"./product/44.jpg"}
                      onAddDonut={addDonut4}
                      onRemoveDonut={remDonut4}
                      numDonuts={numDonuts4}
                    />
                  </RowStyle>
                  <CheckoutForm
                    price={getDonutPrice4(numDonuts4)}
                    onSuccessfulCheckout={() => Router.push("/success")}
                  />
                </Layout>
              </Divs>
            </Col>

            <Col xs="12" sm="12" md="12" lg="4" xl="4">
              <Divs>
                <Layout title="O - store">
                  <RowStyle>
                    <WomanShop
                      info={'Promocja'}
                      titleProduct={'Stringi Szare'}
                      discount={'29.95 zł'}
                      priceProduct={'19.95 zł'}
                      pathImage={"./product/55.jpg"}
                      onAddDonut={addDonut5}
                      onRemoveDonut={remDonut5}
                      numDonuts={numDonuts5}
                    />
                  </RowStyle>
                  <CheckoutForm
                    price={getDonutPrice5(numDonuts5)}
                    onSuccessfulCheckout={() => Router.push("/success")}
                  />
                </Layout>
              </Divs>
            </Col>

            <Col xs="12" sm="12" md="12" lg="4" xl="4">
              <Divs>
                <Layout title="O - store">
                  <RowStyle>
                    <WomanShop
                      info={'Promocja'}
                      titleProduct={'Stringi Fioletowe'}
                      discount={'29.95 zł'}
                      priceProduct={'19.95 zł'}
                      pathImage={"./product/66.jpg"}
                      onAddDonut={addDonut6}
                      onRemoveDonut={remDonut6}
                      numDonuts={numDonuts6}
                    />
                  </RowStyle>
                  <CheckoutForm
                    price={getDonutPrice6(numDonuts6)}
                    onSuccessfulCheckout={() => Router.push("/success")}
                  />
                </Layout>
              </Divs>
            </Col>
          </TreeRowStyle>
        </Row>


        <br />
        <br />
        <br />


        {/* Majtki section */}
        <Row>
          <TreeRowStyle>
            <Col xs="12" sm="12" md="12" lg="4" xl="4">
              <Divs>
                <Layout title="O - store">
                  <RowStyle>
                    <WomanShop
                      titleProduct={'Figi Czarne'}
                      priceProduct={'18.95 zł'}
                      pathImage={"./product/77.jpg"}
                      onAddDonut={addDonut7}
                      onRemoveDonut={remDonut7}
                      numDonuts={numDonuts7}
                    />
                  </RowStyle>
                  <CheckoutForm
                    price={getDonutPrice7(numDonuts7)}
                    onSuccessfulCheckout={() => Router.push("/success")}
                  />
                </Layout>
              </Divs>
            </Col>

            <Col xs="12" sm="12" md="12" lg="4" xl="4">
              <Divs>
                <Layout title="O - store">
                  <RowStyle>
                    <WomanShop
                      titleProduct={'Figi Różowe'}
                      priceProduct={'18.95 zł'}
                      pathImage={"./product/88.jpg"}
                      onAddDonut={addDonut8}
                      onRemoveDonut={remDonut8}
                      numDonuts={numDonuts8}
                    />
                  </RowStyle>
                  <CheckoutForm
                    price={getDonutPrice8(numDonuts8)}
                    onSuccessfulCheckout={() => Router.push("/success")}
                  />
                </Layout>
              </Divs>
            </Col>

            <Col xs="12" sm="12" md="12" lg="4" xl="4">
              <Divs>
                <Layout title="O - store">
                  <RowStyle>
                    <WomanShop
                      titleProduct={'Figi Białe'}
                      priceProduct={'18.95 zł'}
                      pathImage={"./product/99.jpg"}
                      onAddDonut={addDonut9}
                      onRemoveDonut={remDonut9}
                      numDonuts={numDonuts9}
                    />
                  </RowStyle>
                  <CheckoutForm
                    price={getDonutPrice9(numDonuts9)}
                    onSuccessfulCheckout={() => Router.push("/success")}
                  />
                </Layout>
              </Divs>
            </Col>
          </TreeRowStyle>
        </Row>

        
        <br />
        <br />
        <br />

      </Container>


      <Video />
      <About />
      <HowToBuy />
      <Galery />
      <Question />
 
      <Container> 
        <div id="section4">
          <Contact />
        </div>
      </Container>

      <MethodPay />
      
      <Footer />

      <style jsx>{`

        .hiddenMobile {
          display: block;
        } 

        .showMobile {
          display: none;
        }

        .productContent {
          margin-top: 35%;
          // margin-left: 50px;
          width: 80%;
          border: 1px solid red !important;
        }
      

        @media only screen and (max-width: 992px) {
          .showMobile {
            display: block;
          }

          .hiddenMobile {
            display: none;
          } 
        }

      `}</style>
    </>
  );
};

export default MainPage;

