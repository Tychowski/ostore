import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import Footer from '../components/Footer';
import NawigacjaGalery from '../components/NawigacjaGalery';
// import 'bootstrap-css-only/css/bootstrap.min.css';





const Galery = () => {

  return (
    <>
        <NawigacjaGalery />
        <div className="policy">
            <Container>
                <Row>
                    <Col>
                        <div className="titlePolicy">
                            <h1>REGULAMIN SKLEPU INTERNETOWEGO OSTORE</h1>
                        </div>
        
                        <div className="content">
                            I. INFORMACJE O PRZEDSIĘBIORCY
                            <br />

                            1. Niniejszy Regulamin określa ogólne warunki oraz zasady korzystania ze sklepu internetowego zlokalizowanego pod adresem: www.o-store.pl, dalej Sklep Internetowy OSTORE, przez podmioty, w tym konsumentów, nabywających Towary za pośrednictwem Sklepu Internetowego OSTORE.
                            2. Sklep Internetowy OSTORE, działający pod adresem www.o-store.pl, prowadzony jest przez: Libertium Sp. z o.o. z siedzibą w Krakowie, adres: ul. Mogilska 16/7, 31-516 Kraków, spółkę wpisaną do Rejestru Przedsiębiorców Krajowego Rejestru Sądowego pod numerem KRS 0000776711. Kapitał zakładowy 5.000 zł. NIP: 6751698725, REGON: 382836024.
                            
                            <br />
                            <br />
                            <br />
                            II. DEFINICJE
                            <br />

                            Użyte w Regulaminie zwroty oznaczają:
                            1. Libertium Sp. z o.o. z siedzibą w Krakowie, adres: ul. Mogilska 16/7, 31-516 Kraków, spółkę wpisaną do Rejestru Przedsiębiorców Krajowego Rejestru Sądowego pod numerem KRS 0000776711. Kapitał zakładowy 5.000 zł. NIP: 6751698725, REGON: 382836024.
                            2. Sklep Internetowy – zorganizowana platforma informatyczna do prezentacji i sprzedaży Towarów prowadzona przez OSTORE, poprzez stronę internetową www.o-store.pl.
                            3. Klient – podmiot, w tym konsument, nabywający Towary za pośrednictwem Sklepu Internetowego.
                            4. Towar – produkt prezentowany jako przeznaczony do sprzedaży w Sklepie Internetowym.
                            5. Regulamin – niniejszy Regulamin.
                            6. Zamówienie – oferta, nabycia konkretnie oznaczonego Towaru za cenę uwidocznioną w
                            Sklepie Internetowym, kierowana przez Klienta do OSTORE, zgodnie z Regulaminem.
                            
                            <br />
                            <br />
                            <br />
                            III. POSTANOWIENIA OGÓLNE
                            <br />

                            1. OSTORE, za pośrednictwem Sklepu Internetowego, prezentuje oznaczone Towary wraz z ceną jednostkową każdego z nich.
                            2. Informacje o Towarach przeznaczonych do sprzedaży umieszczone w Sklepie Internetowym nie stanowią oferty w rozumieniu art. 66§1 kodeksu cywilnego, lecz cennik oraz zaproszenie do złożenia przez Konsumenta Zamówienia zgodnie z ceną jednostkową Towaru i warunkami Regulaminu.
                            3. Sprzedaż i dostawa Towarów poprzez Sklep Internetowy obejmuje wyłącznie teren Rzeczypospolitej Polskiej.
                            4. Warunkiem korzystania ze Sklepu Internetowego jest zapoznanie się i akceptacja niniejszego Regulaminu.
                            Esotiq otrzymując Zamówienie działa w zaufaniu, iż:
                            Klient ma wymaganą zdolność do czynności prawnej, którą dokonuje; dane i informacje, które Klient podaje OSTORE są zgodne z prawdą; Klient zapoznał się i zaakceptował niniejszy Regulamin;
                            Klienci mogą kontaktować się ze Sklepem Internetowym pod adresem e-mail:
                            7.Celem prawidłowego korzystania ze Sklepu Internetowego korzystający (Klient) powinien dysponować sprzętem komputerowym oraz oprogramowaniem spełniającym następujące, minimalne wymogi: (a) komputer, laptop lub inne urządzenie multimedialne z dostępem do Internetu (w przypadku Aplikacji - urządzenie mobilne); (b) dostęp do poczty elektronicznej; (c) przeglądarka internetowa: Mozilla Firefox w wersji 55.0 i wyższej lub Internet Explorer w wersji 11.0 i wyższej, Opera w wersji 52.0 i wyższej, Google Chrome w wersji 59.0. i wyższej, Safari w wersji 11.0 i wyższej; (d) zalecana minimalna rozdzielczość ekranu: 1280x800; (e) włączenie w przeglądarce internetowej możliwości zapisu plików Cookies oraz obsługi Javascript;. By zawrzeć Umowę Sprzedaży Klient musi posiadać ważny/aktywny adres e-mail, a także w określonych przypadkach klawiaturę lub inne urządzenie wskazujące, umożliwiające poprawne wypełnienie formularzy elektronicznych.
                            
                            <br />
                            <br />
                            <br />
                            IV. TOWARY
                            <br />
                            
                            1. Każdy Towar prezentowany za pośrednictwem Sklepu Internetowego do sprzedaży, zawiera przykładową fotografię charakterystyczną dla jego rodzaju oraz, we właściwej zakładce, informacje o jego właściwościach tj.: kolory i materiały z jakich został wykonany, rozmiary, konkretną sygnaturę Towaru.
                            2. Każdy Towar prezentowany do sprzedaży w ramach Sklepu Internetowego stanowi własność OSTORE, jest fabrycznie nowy, wolny od wad fizycznych i prawnych, oraz dopuszczony do obrotu na terenie Rzeczypospolitej Polskiej.
                            3. Towary prezentowane na fotografiach w Sklepie Internetowym są przykładem Towaru rodzajowo tożsamego i są przedstawione w sposób najbliższy ich faktycznemu wyglądowi. Zdjęcia oddają możliwie najdokładniej kolory i wygląd Towarów. Ze względu na indywidualne ustawienia lub parametry monitora użytkownika mogą zaistnieć niewielkie różnice między zdjęciem a faktycznym kolorem Towaru.
                            
                            <br />
                            <br />
                            <br />
                            V. PRZYJMOWANIE I SKŁADANIE ZAMÓWIEŃ
                            <br />
                            
                            1. Zamówienia w Sklepie Internetowym można składać 24 godziny na dobę, 7 dni w tygodniu wyłącznie poprzez stronę Internetową www.o-store.pl.
                            2. Dokonanie zakupu w Sklepie Internetowym nie wymaga założenia Konta Klienta, natomiast wymagane jest podanie podstawowych danych, umożliwiających kontakt i wysyłkę Towaru.
                            3. Złożenie Zamówienia w Sklepie Internetowym wymaga przejścia przez poszczególne jego etapy. Klient chcąc złożyć Zamówienie, wybiera spośród dostępnych Towarów jego rodzaj, model, kolorystykę, rozmiar, ilość, za cenę wskazaną na Stronie Internetowej. Wybór następuje poprzez okna dialogowe na stronie Sklepu Internetowego, zgodnie z ich funkcjonalnością w tym poprzez kliknięcie ikonki „dodaj do koszyka”. Klient wybiera również sposób płatności. Po dokonaniu wyboru Towaru, podaniu wymaganych danych oraz sposobu płatności Klient poprzez naciśnięcie
                            „KUP” wysyła Zamówienie do OSTORE. Wysłanie poprawnie wypełnionego Zamówienia jest równoznaczne ze złożeniem OSTORE oferty kupna wybranego Towaru zgodnie z cennikiem OSTORE oraz pokrycia kosztów jego dostarczenia.
                            4. Klient po złożeniu Zamówienia, otrzyma, za pośrednictwem poczty e-mail na podany przez siebie adres, informację zwrotną, zawierającą przyjęcie Zamówienia ze wskazaniem ilości, rodzaju i wartości Zamówienia oraz unikalny numer Zamówienia. Otrzymany przez

                            Klienta e-mail jest również potwierdzeniem, że oferta nabycia Towaru została złożona prawidłowo, dotarła do Sklepu Internetowego OSTORE i zgodnie z Regulaminem została przyjęta, a zatem doszło do zawarcia umowy sprzedaży. Informacja o przyjęciu oferty przez OSTORE będzie zawierała potwierdzenie zawarcia umowy, w tym oznaczenie sprzedawcy z jego adresem, datę sprzedaży, określenie Towaru jego ilości, ceny i kosztów dostawy.
                            5. Na każdym etapie składania Zamówienia, do chwili jego wysłania, można je anulować, przerywając proces zamawiania poprzez usunięcie dodanych uprzednio towarów z koszyka. 6. Koniecznym warunkiem realizacji umowy sprzedaży za pośrednictwem Sklepu Internetowego jest podanie prawidłowego podmiotu zamawiającego, adresu do dostarczenia Towaru, numeru telefonu oraz adresu e-mail. W przypadku wadliwych danych, sklep OSTORE zastrzega sobie prawo do kontaktu z Klientem.
                            7. Cenniki Towarów na stronie Sklepu Internetowego mają charakter zmienny. Wiążąca dla Klienta oraz OSTORE jest cena Towaru zgodna z cennikiem z chwili złożenia przez Klienta Zamówienia.
                            8. Maksymalna wartość Towarów objętych jednym Zamówieniem płatnym za pobraniem nie może wynosić więcej niż 3000 złotych (trzy tysiące złotych).
                            
                            <br />
                            <br />
                            <br />
                            VI. CENY TOWARÓW I KOSZTY DOSTAWY
                            <br />

                            1. Cena przy każdym Towarze prezentowanym w Sklepie Internetowym podawana jest w złotych polskich i zawiera podatek VAT, lecz nie zawiera kosztów dostawy.
                            2. Informacja na temat całkowitej wartości zamówienia wraz z kosztami dostawy, przedstawiana jest w Koszyku po dokonaniu przez Klienta wyboru formy płatności.
                            3. Wprowadzone zmiany cen Towarów w Sklepie Internetowym, nie będą wpływać na złożone już przez Klienta i zaakceptowane przez OSTORE Zamówienie.
                            4. Promocje w Sklepie OSTORE nie podlegają łączeniu o ile regulamin danej promocji nie stanowi inaczej.
                            
                            <br />
                            <br />
                            <br />
                            VII. FORMY PŁATNOŚCI
                            <br />

                            Klient Sklepu Internetowego może wybrać następujące formy płatności za zamówione Towary:
                            1. KARTĄ PŁATNICZĄ (w tym kredytową). W takiej sytuacji Klient zostanie przekierowany do serwisu STRIPE.com, należącego do firmy STRIPE z siedzibą w Stanach Zjednoczonych.
                            2. Zamówienie będzie wadliwie złożone i Umowa sprzedaży nie dochodzi do skutku, w każdym przypadku gdy:
                            a. Klient wybrał płatność należności przez serwis STRIPE, a transakcja nie została autoryzowana (zaakceptowana);
                            b. Klient podał nieprawdziwe dane teleadresowe uniemożliwiające prawidłową realizację świadczeń.
                            
                            <br />
                            <br />
                            <br />
                            VIII. CZAS REALIZACJI ZAMÓWIENIA I DOSTAWA
                            <br />

                            1. Wszystkie zaakceptowane Zamówienia dostarczane są do Klienta przez firmę kurierską Poczta Polska S.A. lub operatora pocztowego InPost S.A.
                            2. Przy każdym Towarze, zgodnie z funkcjonalnością Sklepu Internetowego, podany jest maksymalny czas realizacji zamówienia przez Sklep Internetowy, przy czym podany czas obejmuje dni od poniedziałku do piątku. Czas realizacji zamówienia różni się w zależności od wybranego sposobu płatności. Bieg terminu realizacji Zamówienia rozpoczyna się:
                            a. w wypadku płatności KARTĄ PŁATNICZĄ - w momencie otrzymania przez sklep OSTORE autoryzacji wpłaty Klienta w systemie STRIPE;
                            3. Do czasu realizacji zamówienia przez Sklep Internetowy należy doliczyć 1 dzień na doręczenie Towaru.
                            4. Wszyscy klienci będą informowani przy pomocy wiadomości e-mail o aktualnym stanie realizacji zamówienia.
                            5. W przypadku otrzymania przez Klienta Towaru w uszkodzonym opakowaniu, zaleca się opisać ten fakt przy odbiorze przesyłki od kuriera i zgłosić go pracownikowi OSTORE za pośrednictwem poczty e-mail na adres: ostore.polska@gmail.com.
                            8. W przypadku nieprzewidzianych sytuacji ostore będzie informował Klienta o problemie i sposobie jego rozwiązania na adres mailowy podany w Zamówieniu.
                            
                            <br />
                            <br />
                            <br />
                            IX. REKLAMACJA
                            <br />

                            1. OSTORE odpowiada za wady fizyczne i prawne Towarów, zgodnie z obowiązującymi przepisami w tym Ustawą z dnia 23 kwietnia 1964r. Kodeks cywilny (Dz. U. z 2014r. poz. 121) oraz Ustawą z dnia 30 maja 2014r. o prawach konsumenta (Dz.U. z 24 czerwca 2014r., poz. 827).
                            2. W szczególności OSTORE odpowiada za wady fizyczne Towaru, istniejące w chwili przejścia niebezpieczeństwa na Klienta lub wynikające z przyczyny tkwiącej w sprzedanym Towarze w tej samej chwili, polegające na niezgodności Towaru z umową.
                            3. Klient może skorzystać z pozasądowych sposobów rozpatrywania reklamacji i dochodzenia roszczeń. Chcąc skorzystać z możliwości polubownego rozwiązywania sporów dotyczących zakupów internetowych, Konsument może złożyć swoją skargę np. za pośrednictwem unijnej platformy internetowej ODR
                            4. Klient składając reklamację ma możliwość skorzystania z formularza reklamacyjnego.
                            5. W przypadku Reklamacji OSTORE zaleca odesłanie Towaru w oryginalnym
                            opakowaniu, z dołączonym oryginałem dowodu zakupu i opisem reklamacji, na adres OSTORE (Libertium), ul. Mogilska 16/7, 31-516 Kraków z dopiskiem „REKLAMACJA”.
                            6. Sklep OSTORE rozpatrzy reklamacje w ciągu 14 dni od dnia otrzymania reklamacji.
                            7. W przypadku uznania reklamacji, towar wadliwy zostanie, stosownie do woli
                            Konsumenta, naprawiony lub wymieniony na inny, pełnowartościowy, a jeśli będzie to już niemożliwe (na przykład z powodu wyczerpania zapasów towaru) lub wymagałoby to nadmiernych kosztów, wówczas OSTORE w uzgodnieniu z Klientem, zwróci Klientowi równowartość ceny Towaru lub obniży cenę albo też, za zgodą Klienta, zaoferuje mu inne, dostępne w Sklepie towary do wyboru.
                            8. Jeżeli sprzedaż dokonywana jest na rzecz Klienta w ramach transakcji nieodpowiadającej definicji sprzedaży konsumenckiej w rozumieniu ustaw określonych w ust. 1 powyżej, reklamacja rozpatrywana jest zgodnie z odpowiednimi postanowieniami Kodeksu Cywilnego. W takim wypadku szczególne postanowienia dotyczące ochrony konsumentów nie mają zastosowania.

                            10. Pytania oraz powiadomienia dotyczące reklamacji prosimy kierować na adres email: ostore.polska@gmail.com
                            11. Reklamacje dotyczące świadczenia usług drogą elektroniczną (np. problemy z dokonaniem zakupu) prosimy zgłaszać na adres ostore.polska@gmail.com.
                            
                            <br />
                            <br />
                            <br />
                            X. ODSTĄPIENIE OD UMOWY
                            <br />

                            1. Zgodnie z art. 27. Ustawy z dnia 30 maja 2014r. o prawach konsumenta (Dz.U. z 24 czerwca 2014r., poz. 827), Klient będący konsumentem zawierający umowę na odległość ma prawo odstąpić od umowy bez podania przyczyn, w terminie do 14 dni od dnia otrzymania Towaru i niezwłocznie zwrócić kupiony Towar będący w niezmienionym stanie. Oświadczenie o odstąpieniu należy przesłać OSTORE na adres ostore.polska@gmail.com 2. Klient chcący odstąpić od umowy ma prawo skorzystać z następującego (KLIKNIJ) formularza oświadczenia o odstąpieniu od umowy.
                            3. W przypadku odstąpienia od umowy Klient nie ponosi kosztów, za wyjątkiem kosztów odesłania Towaru do Sklepu Internetowego oraz za wyjątkiem kosztów dostarczenia Towaru do salonu OSTORE (w przypadku wyboru opcji zwrotu do salonu OSTORE).
                            4. Przekroczenie 30 dniowego terminu do odstąpienia od umowy będzie podstawą nieuwzględnienia odstąpienia od umowy.
                            5. Zwrot ceny, jeżeli została uiszczona, odbędzie się w sposób wskazany w Rozdziale XI Regulaminu („Zwrot należności klientom”).
                            
                            <br />
                            <br />
                            <br />
                            XI.
                            ZWROT NALEŻNOŚCI KLIENTOM
                            <br />

                            1.
                            a.
                            odpowiednio całość lub część wartości Zamówienia) opłaconego z góry przed jego realizacją;
                            b. zwrotu produktu wskutek odstąpienia od umowy;
                            c. uznania reklamacji i braku możliwości naprawy uszkodzonego produktu lub wymiany na
                            nowy i braku dalszych ustaleń.
                            2. OSTORE dokonuje zwrotu płatności przy użyciu takiego samego sposobu zapłaty, jakiego użył Klient przy zakupie towaru, chyba że Klient wyraźnie zgodził się na inny sposób zwrotu, który nie wiąże się dla niego z żadnymi kosztami.
                            3. Sklep OSTORE dokona zwrot płatności z zachowaniem terminów wynikających z bezwzględnie obowiązujących przepisów prawa.
                            
                            <br />
                            <br />
                            <br />
                            XII. DANE OSOBOWE
                            <br />

                            1. Składając Zamówienie w Sklepie Internetowym (w momencie akceptacji niniejszego regulaminu), Klient wyraża zgodę na umieszczenie swoich danych osobowych w bazie klientów OSTORE oraz na ich przetwarzanie dla celów realizacji umowy. Podanie danych osobowych przez Klienta jest dobrowolne, jednak brak zgody na przetwarzanie danych osobowych uniemożliwia zrealizowanie zamówienia Klienta.
                            2. Klienci sklepu OSTORE oraz pozostali użytkownicy strony internetowej www.o-store.pl mają możliwość, dobrowolnie, wyrażenia zgody na otrzymywanie od Sklepu OSTORE,
                            Sklep OSTORE dokona niezwłocznie zwrotu należności w przypadku:
                            anulowania Zamówienia lub części Zamówienia (w takim przypadku zwrotowi podlega
                            
                            informacji handlowej w rozumieniu ustawy z dnia 18 lipca 2002 r. o świadczeniu usług drogą elektroniczną (Dz.U.02.144.1204 z ze zm.).
                            3. Dane osobowe, pozyskiwane za pośrednictwem strony www.o-store.pl, są chronione zgodnie z ustawą z dnia 10 maja 2018 roku o ochronie danych osobowych, oraz zgodnie z rozporządzeniem Parlamentu Europejskiego i Rady (UE) 2016/679 z dnia 27 kwietnia 2016 r. w sprawie ochrony osób fizycznych w związku z przetwarzaniem danych osobowych i w sprawie swobodnego przepływu takich danych oraz uchylenia dyrektywy 95/46/WE (ogólne rozporządzenie o ochronie danych), zwanym dalej „RODO”, w sposób uniemożliwiający dostęp do nich osób trzecich. Administratorem danych osobowych jest Libertium Sp. z o.o. z siedzibą w Krakowie Klienci dysponują prawem do wglądu i wnoszenia poprawek do swoich danych osobowych oraz ich usunięcia. Dane będą służyły celom zgodnie z udzieloną przez Klienta zgodą tj. do realizacji Zamówienia, a w przypadku zgody na otrzymywanie informacji handlowej, do przesyłania takiej informacji.
                            4. Po wybraniu przez Klienta Towarów, które chce zakupić poprzez Sklep Internetowy OSTORE, system informatyczny obsługujący Sklep Internetowy przedstawi Klientowi informację o ochronie danych osobowych o treści zgodnej z załącznikiem numer 1 do niniejszego Regulaminu, oraz treść niniejszego Regulaminu. W przypadku gdy Klient akceptuje treść niniejszego Regulaminu, oraz wyraża zgodę na przetwarzanie jego danych osobowych dla celów realizacji procesu zamówienia Towaru, wówczas Klient zaznacza (poprzez kliknięcie) pierwszy oraz drugi kwadrat formatki „oświadczenia Klienta Sklepu Internetowego OSTORE” o treści wskazanej w załączniku numer 1 do niniejszego Regulaminu. Zaznaczenie obu powyższych kwadratów jest dobrowolne, a brak akceptacji Regulaminu albo brak zgody na przetwarzanie danych osobowych, jest traktowane jako rezygnacja ze złożonego zamówienia (nie dochodzi do zawarcia umowy sprzedaży ani realizacji zamówienia). Dopiero po zaznaczeniu przez Klienta formatki akceptacji Regulaminu oraz formatki zgody na przetwarzanie danych osobowych – system obsługujący Sklep Internetowy poprosi Klienta o podanie danych osobowych niezbędnych do zawarcia umowy sprzedaży oraz realizacji zamówienia. Klient ma też możliwość zaznaczenia trzeciego kwadratu (zgodę na otrzymywanie drogą elektroniczną informacji handlowych) jednakże zaznaczenie tej zgody jest dobrowolne, a brak zaznaczenia tej formatki nie uniemożliwia realizacji zamówienia.
                            
                            <br />
                            <br />
                            <br />
                            XIII. POSTANOWIENIA KOŃCOWE
                            <br />

                            1. Umowa sprzedaży zawierana jest między Klientem a OSTORE w języku polskim i podlega prawu polskiemu. Utrwalenie, zabezpieczenie i udostępnienie istotnych postanowień zawieranej umowy następuje przez przekazanie klientowi, akceptacji zamówienia w formie e-maila oraz przesłanej wraz z Towarem faktury VAT, która zawiera wszystkie elementy wymagane przez przepisy podatkowe, w tym dane sprzedawcy oraz nabywcy, Towar, cenę, ilość nabytego Towaru. Akceptacja niniejszego regulaminu przez Klienta będącego jednocześnie konsumentem jest równoznaczna ze złożeniem żądania wystawienia faktury dokumentującej sprzedaż.
                            2. Do korzystania z usług Sklepu Klient we własnym zakresie obowiązany jest uzyskać możliwość dostępu do sieci Internet oraz korzystania z urządzeń i wraz z oprogramowaniem koniecznym dla wyświetlania stron www.
                            
                            3. Regulamin niniejszy nie ogranicza praw przyznanych konsumentom przez obowiązujące na terenie Rzeczypospolitej Polskiej ustawodawstwo.
                            4. Wszystkie prawa do zdjęć oraz materiałów video zamieszczonych na stronie www.o-store.pl przysługują OSTORE. Zabrania się wykorzystania ich na jakimkolwiek polu eksploatacji.
                        </div>
                    
                    </Col>
                </Row>
            </Container>
        </div>
        <Footer />
                                            
            
        <style jsx>{` 



            * {
                font-weight: 100;
            }

            .policy {
                background-color: rgb(255, 241, 245);
            }

            .titlePolicy {
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                justify-content: center;
                align-items: center;
                text-align: center;
                margin: 100px 0px 150px;

                font-size: 50px;
                font-family: 'Roboto', sans-serif;
            }

            .contentList {
                font-size: 20px;
                margin-top: 50px;
                margin-bottom: 50px;
            }

            .titleList {
                margin-bottom: 30px;
                margin-top: 30px;
            }

            .content {
                margin-left: 20px;
                margin-bottom: 75px;
            }

            .smallContent {
                margin-left: 60px;
            }

        `}</style>
    </>
  );
}

export default Galery;
