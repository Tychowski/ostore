import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import Footer from '../components/Footer';
import NawigacjaGalery from '../components/NawigacjaGalery';
// import 'bootstrap-css-only/css/bootstrap.min.css';





const Galery = () => {

  return (
    <>
        <NawigacjaGalery />
        <div className="policy">
            <Container>
                <Row>
                    <Col>
                        <div className="titlePolicy">
                            <h1>Dostawa i płatność Ostore.pl</h1>
                        </div>
                        <div className="contentList">
                            <h3 className="title">Informacje o dostawie i płatności:</h3>
                            <ul>
                                <li>1. Zamówienia zrealizowane do 12:00 wysyłamy w tym samym dniu, zamówienia realizowane po 12:00 w kolejnym dniu roboczym.</li>
                                <li>2. Dostawy realizowane są za pośrednictwem Poczty Polskiej (list polecony) lub InPost (paczkomaty).</li>
                                <li>3. Jest możliwość odbioru osobistego na terenie Krakowa.</li>
                                <li>4. Każdy towar zawiera już cenę dostawy.</li>
                                <li>5. Dostępne opcje opłaty zamówienia - Płatność internetowa kartą za pośrednictwem serwisu STRIPE</li>
                            </ul>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
        <Footer />
                                            
            
        <style jsx>{` 

            * {
                font-weight: 100;
            }

            .policy {
                background-color: rgb(255, 241, 245);
            }

            .titlePolicy {
                display: flex;
                flex-direction: row;
                flex-wrap: nowrap;
                justify-content: center;
                align-items: center;
                text-align: center;
                margin-top: 120px;

                font-size: 50px;
                font-family: 'Roboto', sans-serif;
            }

            .title {
                margin-bottom: 50px;
            }

            .contentList {
                font-size: 20px;
                margin-top: 100px;
                margin-bottom: 100px;
            }

        `}</style>
    </>
  );
}

export default Galery;

